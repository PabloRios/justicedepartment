/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Pablo Rios
 */

@Controller
public class MainController {
    
    @RequestMapping("/")
    @ResponseBody
    public String index(){
        String response = "Bienvenido a el <a href='www.portafolio.com'> portafolio :puerto 80</a>";
        return response;
    }
}
