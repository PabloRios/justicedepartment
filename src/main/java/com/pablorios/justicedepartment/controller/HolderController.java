/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.controller;

import com.pablorios.justicedepartment.model.Holder;
import com.pablorios.justicedepartment.service.IServiceHolder;
import com.pablorios.justicedepartment.util.CustomErrorType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Pablo Rios
 */
@Controller
@RequestMapping("/v1")
public class HolderController {

    @Autowired
    IServiceHolder _serviceHolder;

    //<editor-fold defaultstate="collapsed" desc="Create">
    //POST
    @RequestMapping(value = "/consultados", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<String> createHolder(
            @RequestBody Holder holder,
            UriComponentsBuilder ucBuilder
    ) {
        if (holder.getName() == null
                || holder.getName().isEmpty()) {
            return new ResponseEntity(new CustomErrorType("Incapaz de crear. Nombre de consultado vacio."), HttpStatus.CONFLICT);
        }
        if (_serviceHolder.readHolderByName(holder.getName()) != null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de crear. El consultado con nombre "
                    + holder.getName() + " ya existe."), HttpStatus.CONFLICT);
        }
        _serviceHolder.createHolder(holder);
        Holder holderResponse = _serviceHolder.readHolderByName(holder.getName());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                ucBuilder
                        .path("/v1/consultados/{id}")
                        .buildAndExpand(holderResponse.getId())
                        .toUri()
        );
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Read">
    //GET ALL
    @RequestMapping(value = "/consultados", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<List<Holder>> getHolder() {
        List<Holder> holders = _serviceHolder.readAllHolder();
        if (holders.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Holder>>(holders, HttpStatus.OK);
    }

    //GET BY ID
    @RequestMapping(value = "/consultados/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<Holder> getHolderById(
            @PathVariable("id") Long id
    ) {
        if (id == null || id <= 0) {
            return new ResponseEntity(new CustomErrorType("Atributo ID es requerido"), HttpStatus.CONFLICT);
        }

        Holder holder = _serviceHolder.readHolderById(id);
        if (holder == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(holder, HttpStatus.OK);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Update">
    //PATCH 
    @RequestMapping(value = "/consultados/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> updateHolders(@PathVariable("id") Long id, @RequestBody Holder holder) {
        Holder currentHolder = _serviceHolder.readHolderById(id);
        if (currentHolder == null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de actualizar. Tribunal con ID " + id + " no encontrado."),
                    HttpStatus.NOT_FOUND);
        }
        currentHolder.setName(holder.getName());
        _serviceHolder.updateHolder(currentHolder);
        return new ResponseEntity<>(currentHolder, HttpStatus.OK);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Delete">
    //DELETE
    @RequestMapping(value = "/consultados/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseEntity<?> deleteCourse(@PathVariable("id") Long id) {

        Holder holder = _serviceHolder.readHolderById(id);
        if (holder == null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de borrar. Ningún Tribunal registrado con ID " + id + "."),
                    HttpStatus.NOT_FOUND);
        }
        _serviceHolder.deleteHolder(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
//</editor-fold>
}
