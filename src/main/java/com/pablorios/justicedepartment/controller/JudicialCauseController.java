/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.controller;

import com.pablorios.justicedepartment.model.JudicialCause;
import com.pablorios.justicedepartment.service.IServiceJudicialCause;
import com.pablorios.justicedepartment.util.CustomErrorType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Pablo Rios
 */
@Controller
@RequestMapping("/v1")
public class JudicialCauseController {

    @Autowired
    IServiceJudicialCause _serviceJudicialCause;

    //<editor-fold defaultstate="collapsed" desc="Create">
    //POST
    @RequestMapping(value = "/causasjudiciales", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<String> createJudicialCause(
            @RequestBody JudicialCause judicialCause,
            UriComponentsBuilder ucBuilder
    ) {
        if (judicialCause.getRole()== null
                || judicialCause.getRole().isEmpty()) {
            return new ResponseEntity(new CustomErrorType("Incapaz de crear. Rol de la causa judicial vacio."), HttpStatus.CONFLICT);
        }
        if (_serviceJudicialCause.readJudicialCauseById(judicialCause.getId()) != null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de crear. La causa judicial con nombre "
                    + judicialCause.getRole() + " ya existe."), HttpStatus.CONFLICT);
        }
        _serviceJudicialCause.createJudicialCause(judicialCause);
        JudicialCause judicialCauseResponse = _serviceJudicialCause.readJudicialCauseById(judicialCause.getId());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                ucBuilder
                        .path("/v1/causasjudiciales/{id}")
                        .buildAndExpand(judicialCauseResponse.getId())
                        .toUri()
        );
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Read">
    //GET ALL
    @RequestMapping(value = "/causasjudiciales", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<List<JudicialCause>> getJudicialCause() {
        List<JudicialCause> judicialCauses = _serviceJudicialCause.readAllJudicialCause();
        if (judicialCauses.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(judicialCauses, HttpStatus.OK);
    }

    //GET BY ID
    @RequestMapping(value = "/causasjudiciales/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<JudicialCause> getJudicialCauseById(
            @PathVariable("id") Long id
    ) {
        if (id == null || id <= 0) {
            return new ResponseEntity(new CustomErrorType("Atributo ID es requerido"), HttpStatus.CONFLICT);
        }

        JudicialCause judicialCause = _serviceJudicialCause.readJudicialCauseById(id);
        if (judicialCause == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(judicialCause, HttpStatus.OK);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Update">
    //PATCH 
    @RequestMapping(value = "/causasjudiciales/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> updateJudicialCauses(@PathVariable("id") Long id, @RequestBody JudicialCause judicialCause) {
        JudicialCause currentJudicialCause = _serviceJudicialCause.readJudicialCauseById(id);
        if (currentJudicialCause == null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de actualizar. Tribunal con ID " + id + " no encontrado."),
                    HttpStatus.NOT_FOUND);
        }
        currentJudicialCause.setRole(judicialCause.getRole());
        _serviceJudicialCause.updateJudicialCause(currentJudicialCause);
        return new ResponseEntity<>(currentJudicialCause, HttpStatus.OK);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Delete">
    //DELETE
    @RequestMapping(value = "/causasjudiciales/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseEntity<?> deleteCourse(@PathVariable("id") Long id) {

        JudicialCause judicialCause = _serviceJudicialCause.readJudicialCauseById(id);
        if (judicialCause == null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de borrar. Ningún Tribunal registrado con ID " + id + "."),
                    HttpStatus.NOT_FOUND);
        }
        _serviceJudicialCause.deleteJudicialCause(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
//</editor-fold>
}
