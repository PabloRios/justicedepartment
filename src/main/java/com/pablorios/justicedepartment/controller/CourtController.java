/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.controller;

import com.pablorios.justicedepartment.model.Court;
import com.pablorios.justicedepartment.service.IServiceCourt;
import com.pablorios.justicedepartment.util.CustomErrorType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Pablo Rios
 */
@Controller
@RequestMapping("/v1")
public class CourtController {

    @Autowired
    IServiceCourt _serviceCourt;

    //<editor-fold defaultstate="collapsed" desc="Create">
    //POST
    @RequestMapping(value = "/tribunales", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<String> createCourt(
            @RequestBody Court court,
            UriComponentsBuilder ucBuilder
    ) {
        if (court.getName() == null
                || court.getName().isEmpty()) {
            return new ResponseEntity(new CustomErrorType("Incapaz de crear. Nombre de tribunal vacio."), HttpStatus.CONFLICT);
        }
        if (_serviceCourt.readCourtByName(court.getName()) != null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de crear. El tribunal con nombre "
                    + court.getName() + " ya existe."), HttpStatus.CONFLICT);
        }
        _serviceCourt.createCourt(court);
        Court courtResponse = _serviceCourt.readCourtByName(court.getName());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                ucBuilder
                        .path("/v1/tribunales/{id}")
                        .buildAndExpand(courtResponse.getId())
                        .toUri()
        );
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Read">
    //GET ALL
    @RequestMapping(value = "/tribunales", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<List<Court>> getCourt() {
        List<Court> courts = _serviceCourt.readAllCourt();
        if (courts.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(courts, HttpStatus.OK);
    }

    //GET BY ID
    @RequestMapping(value = "/tribunales/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<Court> getCourtById(
            @PathVariable("id") Long id
    ) {
        if (id == null || id <= 0) {
            return new ResponseEntity(new CustomErrorType("Atributo ID es requerido"), HttpStatus.CONFLICT);
        }

        Court court = _serviceCourt.readCourtById(id);
        if (court == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(court, HttpStatus.OK);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Update">
    //PATCH 
    @RequestMapping(value = "/tribunales/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> updateCourts(@PathVariable("id") Long id, @RequestBody Court court) {
        Court currentCourt = _serviceCourt.readCourtById(id);
        if (currentCourt == null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de actualizar. Tribunal con ID " + id + " no encontrado."),
                    HttpStatus.NOT_FOUND);
        }
        currentCourt.setName(court.getName());
        _serviceCourt.updateCourt(currentCourt);
        return new ResponseEntity<>(currentCourt, HttpStatus.OK);
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Delete">
    //DELETE
    @RequestMapping(value = "/tribunales/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseEntity<?> deleteCourse(@PathVariable("id") Long id) {

        Court court = _serviceCourt.readCourtById(id);
        if (court == null) {
            return new ResponseEntity(new CustomErrorType("Incapaz de borrar. Ningún Tribunal registrado con ID " + id + "."),
                    HttpStatus.NOT_FOUND);
        }
        _serviceCourt.deleteCourt(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
//</editor-fold>
    
}
