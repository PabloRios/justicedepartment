/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.service;

import com.pablorios.justicedepartment.model.TipCause;
import java.util.List;

/**
 *
 * @author Pablo Rios
 */
public interface IServiceTipCause {
    
    void createTipCause(TipCause tipCause);
    
    List<TipCause> readAllTipCause();
    
    TipCause readTipCauseById(int id);
    
    TipCause readTipCauseByName(String name);
    
    void updateTipCause(TipCause tipCause);
    
    void deleteTipCause(int id);
}
