/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.service;

import com.pablorios.justicedepartment.dao.IDaoCourt;
import com.pablorios.justicedepartment.dao.IDaoHolder;
import com.pablorios.justicedepartment.model.Holder;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Pablo Rios
 */
@Service("serviceHolder")
@Transactional
public class ServiceHolder implements IServiceHolder {
    
    @Autowired
    private IDaoHolder _daoHolder;
    
    @Override
    public void createHolder(Holder holder) {
        _daoHolder.createHolder(holder);
    }
    
    @Override
    public List<Holder> readAllHolder() {
        return _daoHolder.readAllHolder();
    }
    
    @Override
    public Holder readHolderById(long id) {
        return _daoHolder.readHolderById(id);
    }
    
    @Override
    public Holder readHolderByName(String name) {
        return _daoHolder.readHolderByName(name);
    }
    
    @Override
    public void updateHolder(Holder holder) {
        _daoHolder.updateHolder(holder);
    }
    
    @Override
    public void deleteHolder(long id) {
        _daoHolder.deleteHolder(id);
    }
    
}
