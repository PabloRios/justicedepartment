/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.service;

import com.pablorios.justicedepartment.model.Holder;
import java.util.List;

/**
 *
 * @author Pablo Rios
 */
public interface IServiceHolder {
    void createHolder(Holder holder);
    
    List<Holder> readAllHolder();
    
    Holder readHolderById(long id);
    
    Holder readHolderByName(String name);
    
    void updateHolder(Holder holder);
    
    void deleteHolder(long id);
}
