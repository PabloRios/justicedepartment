/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.service;

import com.pablorios.justicedepartment.dao.IDaoTipCause;
import com.pablorios.justicedepartment.model.TipCause;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Pablo Rios
 */
@Service("serviceTipCause")
@Transactional
public class ServiceTipCause implements IServiceTipCause {

    @Autowired
    private IDaoTipCause _daoTipCause;

    @Override
    public void createTipCause(TipCause tipCause) {
        _daoTipCause.createTipCause(tipCause);
    }

    @Override
    public List<TipCause> readAllTipCause() {
        return _daoTipCause.readAllTipCause();
    }

    @Override
    public TipCause readTipCauseById(int id) {
        return _daoTipCause.readTipCauseById(id);
    }

    @Override
    public TipCause readTipCauseByName(String name) {
        return _daoTipCause.readTipCauseByName(name);
    }

    @Override
    public void updateTipCause(TipCause tipCause) {
        _daoTipCause.updateTipCause(tipCause);
    }

    @Override
    public void deleteTipCause(int id) {
        _daoTipCause.deleteTipCause(id);
    }

}
