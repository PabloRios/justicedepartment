/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.service;

import com.pablorios.justicedepartment.dao.IDaoHolder;
import com.pablorios.justicedepartment.dao.IDaoJudicialCause;
import com.pablorios.justicedepartment.model.JudicialCause;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Pablo Rios
 */
@Service("serviceJudicialCause")
@Transactional
public class ServiceJudicialCause implements IServiceJudicialCause {

    @Autowired
    private IDaoJudicialCause _daoJudicialCause;

    @Override
    public void createJudicialCause(JudicialCause judicialCause) {
        _daoJudicialCause.createJudicialCause(judicialCause);
    }

    @Override
    public List<JudicialCause> readAllJudicialCause() {
        return _daoJudicialCause.readAllJudicialCause();
    }

    @Override
    public JudicialCause readJudicialCauseById(long id) {
        return _daoJudicialCause.readJudicialCauseById(id);
    }

    @Override
    public void updateJudicialCause(JudicialCause judicialCause) {
        _daoJudicialCause.updateJudicialCause(judicialCause);
    }

    @Override
    public void deleteJudicialCause(long id) {
        _daoJudicialCause.deleteJudicialCause(id);
    }

}
