/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.service;

import com.pablorios.justicedepartment.model.JudicialCause;
import java.util.List;

/**
 *
 * @author Pablo Rios
 */
public interface IServiceJudicialCause {
    
    void createJudicialCause(JudicialCause judicialCause);
    
    List<JudicialCause> readAllJudicialCause();
    
    JudicialCause readJudicialCauseById(long id);    
    
    void updateJudicialCause(JudicialCause judicialCause);
    
    void deleteJudicialCause(long id);
}
