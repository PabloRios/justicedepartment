/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.service;

import com.pablorios.justicedepartment.dao.IDaoCourt;
import com.pablorios.justicedepartment.model.Court;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Pablo Rios
 */

@Service("serviceCourt")
@Transactional
public class ServiceCourt implements IServiceCourt {

    @Autowired
    private IDaoCourt _daoCourt;

    @Override
    public void createCourt(Court court) {
        _daoCourt.createCourt(court);
    }

    @Override
    public List<Court> readAllCourt() {
        return _daoCourt.readAllCourt();
    }

    @Override
    public Court readCourtById(long id) {
        return _daoCourt.readCourtById(id);
    }

    @Override
    public Court readCourtByName(String name) {
        return _daoCourt.readCourtByName(name);
    }

    @Override
    public void updateCourt(Court court) {
        _daoCourt.updateCourt(court);
    }

    @Override
    public void deleteCourt(long id) {
        _daoCourt.deleteCourt(id);
    }

}
