/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.dao;

import com.pablorios.justicedepartment.model.Court;
import com.pablorios.justicedepartment.model.TipCause;
import java.util.List;

/**
 *
 * @author Pablo Rios
 */
public interface IDaoTipCause {
    
    void createTipCause(TipCause tipCause);
    
    List<TipCause> readAllTipCause();
    
    TipCause readTipCauseById(int id);
    
    TipCause readTipCauseByName(String name);
    
    void updateTipCause(TipCause tipCause);
    
    void deleteTipCause(int id);
}
