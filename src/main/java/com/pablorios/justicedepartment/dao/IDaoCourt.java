/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.dao;

import com.pablorios.justicedepartment.model.Court;
import java.util.List;

/**
 *
 * @author Pablo Rios
 */
public interface IDaoCourt {
    
    void createCourt(Court court);
    
    List<Court> readAllCourt();
    
    Court readCourtById(long id);
    
    Court readCourtByName(String name);
    
    void updateCourt(Court court);
    
    void deleteCourt(long id);
}
