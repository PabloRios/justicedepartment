/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.dao;

import com.pablorios.justicedepartment.model.Holder;
import java.util.List;

/**
 *
 * @author Pablo Rios
 */
public interface IDaoHolder {
    
    void createHolder(Holder holder);
    
    List<Holder> readAllHolder();
    
    Holder readHolderById(long id);
    
    Holder readHolderByName(String name);
    
    void updateHolder(Holder holder);
    
    void deleteHolder(long id);
}
