/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.dao;

import com.pablorios.justicedepartment.model.JudicialCause;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Pablo Rios
 */
@Repository
@Transactional
public class DaoJudicialCause extends AbstractSession implements IDaoJudicialCause {

    @Override
    public void createJudicialCause(JudicialCause judicialCause) {
        getSession().persist(judicialCause);
    }

    @Override
    public List<JudicialCause> readAllJudicialCause() {
        List judicialCauses = getSession().createQuery("from JudicialCause").list();
        for (Object object : judicialCauses) {
            JudicialCause judicialCause = (JudicialCause) object;
            judicialCause.getCourt();
            judicialCause.getHolder();
            judicialCause.getTipCause();
        }
        return judicialCauses;
    }

    @Override
    public JudicialCause readJudicialCauseById(long id) {
        JudicialCause judicialCause = getSession().get(JudicialCause.class, id);
        judicialCause.getCourt();
        judicialCause.getHolder();
        judicialCause.getTipCause();
        return judicialCause;
    }

    @Override
    public void updateJudicialCause(JudicialCause judicialCause) {
        getSession().update(judicialCause);
    }

    @Override
    public void deleteJudicialCause(long id) {
        JudicialCause judicialCause = readJudicialCauseById(id);
        if (judicialCause != null) {
            getSession().delete(judicialCause);
        }
    }

}
