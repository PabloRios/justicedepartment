/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.dao;

import com.pablorios.justicedepartment.model.TipCause;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Pablo Rios
 */
@Repository
@Transactional
public class DaoTipCause extends AbstractSession implements IDaoTipCause {

    @Override
    public void createTipCause(TipCause tipCause) {
        getSession().persist(tipCause);
    }

    @Override
    public List<TipCause> readAllTipCause() {
        return getSession().createQuery("from TipCause").list();
    }

    @Override
    public TipCause readTipCauseById(int id) {
        return getSession().get(TipCause.class, id);
    }

    @Override
    public TipCause readTipCauseByName(String name) {
        return (TipCause) getSession().createQuery("from TipCause where name = :name")
                .setParameter("name", name)
                .uniqueResult();
    }

    @Override
    public void updateTipCause(TipCause tipCause) {
        getSession().update(tipCause);
    }

    @Override
    public void deleteTipCause(int id) {
        TipCause tipCause = readTipCauseById(id);
        if (tipCause != null) {
            getSession().delete(tipCause);
        }
    }
}
