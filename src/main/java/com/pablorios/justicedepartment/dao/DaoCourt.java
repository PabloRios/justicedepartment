/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.dao;

import com.pablorios.justicedepartment.model.Court;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Pablo Rios
 */
@Repository
@Transactional
public class DaoCourt extends AbstractSession implements IDaoCourt {

    @Override
    public void createCourt(Court court) {
        getSession().persist(court);
    }

    @Override
    public List<Court> readAllCourt() {
        List<Court> list = (List<Court>) getSession().createQuery("from Court").list();
        for (Court court : list) {
            court.getJudicialCauses().size();
        };
        return list;
    }

    @Override
    public Court readCourtById(long id) {
        Court get = getSession().get(Court.class, id);
        get.getJudicialCauses().size();
        return get;
    }

    @Override
    public Court readCourtByName(String name) {
        Court get = (Court) getSession().createQuery("from Court where name = :name")
                .setParameter("name", name)
                .uniqueResult();
        get.getJudicialCauses().size();
        return get;        
    }

    @Override
    public void updateCourt(Court court) {
        getSession().update(court);
    }

    @Override
    public void deleteCourt(long id) {
        Court court = readCourtById(id);
        if (court != null) {
            getSession().delete(court);
        }
    }

}
