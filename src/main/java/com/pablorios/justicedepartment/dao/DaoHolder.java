/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablorios.justicedepartment.dao;

import com.pablorios.justicedepartment.model.Holder;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Pablo Rios
 */
@Repository
@Transactional
public class DaoHolder extends AbstractSession implements IDaoHolder {

    @Override
    public void createHolder(Holder holder) {
        getSession().persist(holder);
    }

    @Override
    public List<Holder> readAllHolder() {
        List<Holder> list = (List<Holder>) getSession().createQuery("from Holder").list();
        for (Holder holder : list) {
            holder.getJudicialCauses().size();
        };
        return list;
    }

    @Override
    public Holder readHolderById(long id) {
        Holder get = getSession().get(Holder.class, id);
        get.getJudicialCauses().size();
        return get;
    }

    @Override
    public Holder readHolderByName(String name) {
        Holder holder = (Holder) getSession().createQuery("from Holder where name = :name")
                .setParameter("name", name)
                .uniqueResult();
        holder.getJudicialCauses().size();
        return holder;
    }

    @Override
    public void updateHolder(Holder holder) {
        getSession().update(holder);
    }

    @Override
    public void deleteHolder(long id) {
        Holder holder = readHolderById(id);
        if (holder != null) {
            getSession().delete(holder);
        }
    }
}
