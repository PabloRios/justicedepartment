package com.pablorios.justicedepartment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JusticeDepartmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(JusticeDepartmentApplication.class, args);
	}
}
