package com.pablorios.justicedepartment.model;
// Generated 22-04-2018 18:51:06 by Hibernate Tools 4.3.1

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Court generated by hbm2java
 */
@Entity
@Table(name = "court",
        catalog = "justice_department"
)
public class Court implements java.io.Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "name", length = 250)
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "court_id")
    @JsonIgnore
    private Set<JudicialCause> judicialCauses = new HashSet(0);

    

    public Court() {
    }

    public Court(long id, String name, Set<JudicialCause> judicialCauses) {
        this.id = id;
        this.name = name;
        this.judicialCauses = judicialCauses;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<JudicialCause> getJudicialCauses() {
        return judicialCauses;
    }

    public void setJudicialCauses(Set<JudicialCause> judicialCauses) {
        this.judicialCauses = judicialCauses;
    }

}
